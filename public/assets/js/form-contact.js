/* ------ FORM AJAX ------ */
window.addEventListener('load', init);
function init(){
    document.querySelector('#contact-form').addEventListener('submit', sendForm, false);
}
function sendForm(e){
    e.preventDefault();
    var form = this;
    var fD = new FormData(form);
    var ajax = new XMLHttpRequest();
    ajax.open('POST', 'contact.php', true);
    ajax.responseType = 'text';
    ajax.addEventListener('load', function(e){
        if(this.status = 200){
            $("#contact-form").hide("fast");
            console.log(this.response);
            $("#success-form").show("fast");
        }else {
            console.log("No funciona");
        }
    }, false);
    ajax.send(fD);
    return false;
}
/* ------ END FORM AJAX ------ */