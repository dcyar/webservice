<!DOCTYPE html>
<html lang="es">

<head>
        <!-- Meta -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" /> 
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- SITE TITLE -->
        <title>Awesome</title>         
        <!-- Latest Bootstrap min CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />       
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700' rel='stylesheet' type='text/css' />
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome.min.css') }}" />
        <!--- owl carousel Css-->
        <link rel="stylesheet" href="{{ asset('assets/owlcarousel/css/owl.carousel.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/owlcarousel/css/owl.theme.css') }}" />             
        <!-- venobox CSS -->        
        <link rel="stylesheet" href="{{ asset('assets/css/venobox.css') }}" />               
        <!-- animate CSS -->        
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" />       
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" /> 
        <!-- CSS FOR COLOR SWITCHER -->
        <link rel="stylesheet" href="{{ asset('assets/css/switcher/switcher.css') }}" />     
        <link rel="stylesheet" href="{{ asset('assets/css/switcher/style4.css') }}" id="colors" />       
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body data-spy="scroll" data-offset="80">

        <!-- START NAVBAR -->
        <div class="navbar navbar-default navbar-fixed-top menu-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand"><img src="assets/img/logo.png" alt="DyamCode"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <nav>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="#home">INICIO</a></li>
                            <li><a class="page-scroll" href="#service">SERVICIOS</a></li>
                            <li><a class="page-scroll" href="#pricing">PRECIOS</a></li>
                            <li><a class="page-scroll" href="#portfolio">PORTAFOLIO</a></li>                
                            <li><a class="page-scroll" href="#testimonials">TESTIMONIOS</a></li>
                            <li><a class="page-scroll" href="#contact">CONTACTO</a></li>
                        </ul>
                    </nav>
                </div> 
            </div><!--- END CONTAINER -->
        </div> 
        <!-- END NAVBAR --> 
        
        <!-- START HOME DESIGN -->
    
        <!-- START HOME -->
        <section id="home" class="welcome-area">
            <div class="welcome-slider-area">
                <div id="welcome-slide-carousel" class="carousel slide carousel-fade" data-ride="carousel">
    
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="single-slide-item slide-1">
                                <div class="single-slide-item-table">
                                    <div class="single-slide-item-tablecell">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h2>desarrollamos sitios web responsive 100% profesionales</h2>
                                                    <p>Con las últimas tendencias tecnológicas, para que tu sitio web encante a tus clientes.</p>
                                                    <a class="btn btn-default btn-home-bg page-scroll" href="#contact">contratar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="single-slide-item slide-2">
                                <div class="single-slide-item-table">
                                    <div class="single-slide-item-tablecell">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h2>hosting, dominios y correos corporativos</h2>
                                                    <p>Obtén tu hosting al mejor precio, dominios <strong>.com .pe .net .org</strong> y correos corporativos.</p>
                                                    <a class="btn btn-default btn-home-bg page-scroll" href="#contact">contratar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="single-slide-item slide-3">
                                <div class="single-slide-item-table">
                                    <div class="single-slide-item-tablecell">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h2>social media y marketing</h2>
                                                    <p>Genera presencia en las Redes Sociales y amplia tu mercado.</p>
                                                    <a class="btn btn-default btn-home-bg page-scroll" href="#contact">contratar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                 <!-- Controls -->
                  <a class="left carousel-control" href="#welcome-slide-carousel" role="button" data-slide="prev">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                  </a>
                  <a class="right carousel-control" href="#welcome-slide-carousel" role="button" data-slide="next">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                  </a>
                </div>
            </div>
        </section>
        <!-- END  HOME DESIGN -->   

        <!-- START FEATURE-->
        <section id="about" class="feature">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-md-3 col-sm-6 col-xs-12 single_about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <i class="fa fa-heart"></i>
                        <h4>Dedicación</h4>
                        <p>Amámos nuestro trabajo y ponemos todo nuestro empeño en la creación de tu sitio.</p>                     
                    </div><!--- END COL -->
                    <div class="col-md-3 col-sm-6 col-xs-12 single_about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s" data-wow-offset="0">    
                        <i class="fa fa-life-ring"></i>
                        <h4>Compromiso</h4>
                        <p>Nuestro compromiso contigo es muy importante.</p>
                    </div><!--- END COL -->
                    <div class="col-md-3 col-sm-6 col-xs-12 single_about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">        
                        <i class="fa fa-paper-plane-o"></i>
                        <h4>Efectividad</h4>
                        <p>Creamos el sitio web perfecto para que vendas tus productos o servicios y amplies tu mercado.</p>
                    </div><!--- END COL -->
                    <div class="col-md-3 col-sm-6 col-xs-12 single_about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
                        <i class="fa fa-hourglass-end"></i>
                        <h4>optimización</h4>
                        <p>Construimos tu sitio en menos de una semana, responsive, a medida y todo lo que necesites.</p>
                    </div><!--- END COL -->                     
                </div><!--- END ROW -->                             
            </div><!--- END CONTAINER -->
        </section>
        <!-- END FEATURE -->
        
        <!-- PROMOTION -->
        <section class="buy_now">
            <div class="container text-center">
                <h3 class="buy_now_title">contrata el plan <strong>premium</strong> y ahorra el 10%<a href="#contact" class="btn btn-default page-scroll btn-promotion-bg">contratar</a> </h3>
            </div><!--- END CONTAINER -->
        </section>
        <!-- END PROMOTION -->

        <!-- START SERVICE  -->
        <section id="service" class="our_service section-padding">
            <div class="container">
                <div class="row text-center">
                    <div class="section-title wow zoomIn">
                        <h2>nuestros <span>servicios</span></h2>
                        <div></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-desktop"></i>
                            <h4>Desarrollo web</h4>
                            <p>Desarrollamos sitios web profesionales, responsives y optimizados.</p>
                        </div>
                    </div><!--END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-bullhorn"></i>
                            <h4>Social Media</h4>
                            <p>Generamos presencia en redes sociales e internet.</p>
                        </div>
                    </div><!--END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-briefcase"></i>
                            <h4>Marketing Digital</h4>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
                        </div>
                    </div><!--END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-camera-retro"></i>
                            <h4>Super Ideas</h4>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
                        </div>
                    </div><!--END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-server"></i>
                            <h4>Hosting</h4>
                            <p>Servicio profesional de hosting de calidad y seguro - <strong>24/7</strong>.</p>
                        </div>
                    </div><!--END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12 no-padding wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="single_service">
                            <i class="fa fa-globe"></i>
                            <h4>Dominios</h4>
                            <p>Dominios para tu empresa/organización <strong>.com .pe .org .net</strong></p>
                        </div>
                    </div><!--END COL -->
                </div><!--END  ROW -->
            </div><!-- END CONTAINER -->
        </section>
        <!-- END SERVICE-->     

        <!-- START PRICING -->
        <section id="pricing" class="pricing_table section-padding">
            <div class="container">
                <div class="row text-center">
                    <div class="section-title wow zoomIn">
                        <h2>nuestros <span>planes</span></h2>
                        <div></div>
                    </div>      
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="pricing-table">
                            <h3>Básico</h3>
                            <div class="price color-one">
                                <span>S/.249</span>
                                luego S/.75
                            </div>
                            <ul class="pricing-list">
                                 <li>Sitio web</li>
                                 <li>01 Dominio .com</li>
                                 <li>Hosting 500Mb</li>
                                 <li>Social Media</li>
                                 <li>10 Correos Corporativos</li>
                                 <li>Soporte 24/7</li>
                            </ul>
                            <div class="pricing-btn">
                                <a class="page-scroll btn-light-bg" href="#contact">Contratar</a>
                            </div>
                        </div><!-- END PRICING TABLE -->
                    </div><!-- END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="pricing-table">
                            <h3>Estándar</h3>
                            <div class="price color-two">
                                <span>S/.299</span>
                                luego S/.100
                            </div>
                            <ul class="pricing-list">
                                 <li>Sitio web</li>
                                 <li>01 Dominio .com</li>
                                 <li>Hosting 1000Mb</li>
                                 <li>Social Media</li>
                                 <li>15 Correos Corporativos</li>
                                 <li>Soporte 24/7</li>
                            </ul>
                            <div class="pricing-btn">
                                <a class="page-scroll btn-light-bg" href="#contact">Contratar</a>
                            </div>
                        </div><!-- END PRICING TABLE -->
                    </div><!-- END COL -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="pricing-table">
                            <h3>Premium</h3>
                            <div class="price color-three">
                                <span>S/.349</span>
                                luego S/.125
                            </div>
                            <ul class="pricing-list">
                                 <li>Sitio web</li>
                                 <li>01 Dominio .com</li>
                                 <li>Hosting 2500Mb</li>
                                 <li>Social Media</li>
                                 <li>20 Correos Corporativos</li>
                                 <li>Soporte 24/7</li>
                            </ul>
                            <div class="pricing-btn">
                                <a class="page-scroll btn-light-bg" href="#contact">Contratar</a>
                            </div>
                        </div><!-- END PRICING TABLE -->
                    </div><!-- END COL  -->  
                </div><!--END  ROW  -->
            </div><!-- END CONTAINER  -->
        </section>
        <!-- END PRICING -->

        <!-- START PORTFOLIO -->    
        <section id="portfolio" class="works_area section-padding"> 
            <div class="container">
                <div class="row text-center">
                    <div class="section-title wow zoomIn">
                        <h2>Nuestro <span>Portafolio</span></h2>
                        <div></div>
                    </div>
                    <div class="col-md-12" style="display:none;">
                        <div class="our_work_menu">
                            <ul>
                                <li class="filter wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".25s" data-filter="all">All</li>
                                <li class="filter" data-filter=".branding">Branding</li>
                                <li class="filter" data-filter=".web">Web</li>
                                <li class="filter" data-filter=".package">Package</li>
                                <li class="filter" data-filter=".video">Video</li>
                            </ul>
                        </div>              
                    </div>
                    
                    <div class="work_all_item">
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 mix all branding package">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>                                
                                        <img src="assets/img/portfolio/1.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content">                                             
                                                    <p>Branding</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/1.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>  
                                    </figure>
                                </div>              
                            </div>
                        </div>
                        
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 mix all web">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>
                                        <img src="assets/img/portfolio/2.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content">
                                                    <p>Webdesign</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/2.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>                                          
                                        </div>  
                                    </figure>
                                </div>              
                            </div>
                        </div>
                        
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 all package branding">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>
                                        <img src="assets/img/portfolio/3.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content">
                                                    <p>Package</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/3.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>                                      
                                        </div>                              
                                    </figure>
                                </div>              
                            </div>
                        </div>
                        
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 mix all branding package video">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>
                                        <img src="assets/img/portfolio/4.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content"> 
                                                    <p>Video</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/4.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>                                      
                                        </div>                              
                                    </figure>
                                </div>              
                            </div>
                        </div>                  
                        
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 mix all branding package">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>
                                        <img src="assets/img/portfolio/5.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content">
                                                    <p>Package</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/5.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>                                      
                                        </div>                              
                                    </figure>
                                </div>              
                            </div>
                        </div>                  
                        
                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 mix all branding video">
                            <div class="single_our_work">
                                <div class="sing_work_photo">
                                    <figure>
                                        <img src="assets/img/portfolio/6.jpg" alt="img">
                                        <div class="sing_work_text_link">
                                            <div class="sing_work_content_wrap">
                                                <div class="sing_work_content">
                                                    <p>Branding</p>
                                                    <h4>Portfolio Title</h4>
                                                    <div class="sing_link_img">
                                                        <a href="assets/img/portfolio/6.jpg" class="lightbox search" data-gall="gall-work"><i class="fa fa-eye"></i></a>
                                                        <a href="single_project.html" class="link"><i class="fa fa-link"></i></a>
                                                    </div>  
                                                </div>
                                            </div>                                      
                                        </div>                                                                  
                                    </figure>
                                </div>          
                            </div>
                        </div>                                          
                    </div>              
                </div>
                <div class="portfolio_btn text-center" style="display:none;">
                    <a href="#" class="btn-light-bg">See More Work</a>
                </div>              
            </div>          
        </section>      
        <!-- END PORTFOLIO -->  

        <!-- Start testimonials -->
        <section id="testimonials" class="testimonials_area" style="background-image: url(assets/img/bg/testimonial-bg.jpg);  background-size:cover; background-position: center center;background-attachment:fixed;">
            <div class="testimonial_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                            <div class="testimonials_carousel text-center">
                                <div class="single_testimonial">    
                                    <div class="avatar">
                                        <img src="assets/img/testimonial/1.jpg" alt="" />
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                                    </p>
                                    <h5>Frank A. Brown</h5>
                                </div>          
                                
                                <div class="single_testimonial">
                                    <div class="avatar">
                                        <img src="assets/img/testimonial/2.jpg" alt="" />
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                                    </p>
                                    <h5>Frank A. Brown</h5>
                                </div>      

                                <div class="single_testimonial">
                                    <div class="avatar">
                                        <img src="assets/img/testimonial/3.jpg" alt="" />
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                                    </p>
                                    <h5>Frank A. Brown</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End testimonials -->   
        
        <!-- START COMPANY PARTNER LOGO  -->
        <div class="partner-logo section-padding">
            <div class="partner_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="partner wow fadeInRight">
                                <a href="#" target="_blank"><img src="assets/img/partner/1.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/2.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/3.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/4.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/5.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/6.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/1.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/2.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/3.png" alt="image"></a>
                                <a href="#" target="_blank"><img src="assets/img/partner/4.png" alt="image"></a>
                            </div>
                        </div><!-- END COL  -->
                    </div><!--END  ROW  -->
                </div><!-- END CONTAINER  -->
            </div><!-- END OVERLAY -->
        </div>
        <!-- END COMPANY PARTNER LOGO -->
        
        <!-- START CONTACT FORM-->
        <section id="contact" class="contact_area section-padding">
            <div class="container">
                <div class="row">       
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="contact_address">
                            <h3>Contáctanos</h3>
                            <p>¿Quieres saber mas acerca de nosotros y nuestro servicio?. Ponte en contácto con nosotros y resolveremos todas tus dudas.</p>
                            <ul>
                                <li><i class="fa fa-rocket"></i>Av. Simpre Viva nº 742 - Arequipa</li>
                                <li><i class="fa fa-phone"></i>#999888777 - 111222333</li>
                                <li><i class="fa fa-envelope"></i>info@dyamcode.com</li>
                                <li><i class="fa fa-clock-o"></i>Lunes - Sábado: 08.00am. - 08.00pm.</li>
                            </ul>
                        </div>
                    </div><!-- END COL -->                      
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight">
                        <div class="contact">
                            <div class="success" id="success-form" style="display:none;">Gracias, tu email fue enviado correctamente.</div>
                            <form id="contact-form" method="post" enctype="multipart/form-data">
                                <label>Los campos con <strong>*</strong> son obligatorios</label>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="*Nombres y Apellidos" required="required">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="email" name="email" class="form-control" id="email" placeholder="*Correo electrónico" required="required">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="tel" name="phone" class="form-control" id="phone" placeholder="*Teléfono" required="required">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <select name="plan" class="form-control select-input-setuser">
                                          <option selected="selected" disabled="disabled">¿Te interesa algún plan en plarticular? (opcional)</option>
                                          <option value="Plan Basico">Plan Básico</option>
                                          <option value="Plan Estandar">Plan Estandar</option>
                                          <option value="Plan Premium">Plan Premium</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <textarea rows="6" name="message" class="form-control" id="message" placeholder="*Tu mensaje" required="required"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="actions">
                                            <input type="submit" value="Enviar Mensaje" name="submit" id="submitButton" class="btn btn-lg btn-contact-bg" title="Enviar tu mensaje!" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- END COL -->
                </div><!--- END ROW -->             
            </div><!--- END CONTAINER -->   
        </section>
        <!-- END CONTACT FORM -->
        
        <!-- START MAP -->
        <!-- map js -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2fdjxrrQHcRRoeOZVx3sbBehVQusPeUk"></script>
        <script>
            function initialize() {
              var mapOptions = {
                    zoom: 15,
                    scrollwheel: false,
                    center: new google.maps.LatLng(-16.4040516, -71.5565211)
              };
              var map = new google.maps.Map(document.getElementById('map'),
                  mapOptions);
              var marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: 'assets/img/map_pin.png',
                map: map
              });
            }
            google.maps.event.addDomListener(window, 'load', initialize);               
        </script>
        <div id="map"></div>
        <!-- END MAP -->
        
        <!-- START FOOTER -->
        <footer class="footer section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center wow zoomIn">
                        <div class="footer_content">
                            <a href="/"><img src="assets/img/logo.png" alt="DyamCode" /></a>
                            <div class="footer_social">
                                <ul>
                                    <li><a class="f_facebook  wow bounceInDown" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="f_twitter wow bounceInDown" data-wow-delay=".1s" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="f_google wow bounceInDown" data-wow-delay=".2s" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a class="f_linkedin wow bounceInDown" data-wow-delay=".3s" href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a class="f_youtube wow bounceInDown" data-wow-delay=".4s" href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a class="f_skype wow bounceInDown" data-wow-delay=".5s" href="#"><i class="fa fa-skype"></i></a></li>
                                </ul>
                            </div>  
                            <p>DyamCode &copy; 2016 All Rights Reserved.</p>
                        </div>
                                                
                    </div><!--- END COL -->
                </div><!--- END ROW -->
            </div><!--- END CONTAINER -->   
        </footer>
        <!-- END FOOTER -->                      

        <!-- Latest jQuery -->
            <script src="{{ asset('assets/js/jquery-1.12.4.min.js') }}"></script>
        <!-- Latest compiled and minified Bootstrap -->
            <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- modernizr JS -->       
            <script src="{{ asset('assets/js/modernizr-2.8.3.min.js') }}"></script>            
        <!-- stellar js -->
            <script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>     
        <!-- countTo js -->
            <script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>      
        <!-- owl-carousel min js  -->
            <script src="{{ asset('assets/owlcarousel/js/owl.carousel.min.js') }}"></script>       
        <!-- jquery mixitup min js -->
            <script src="{{ asset('assets/js/jquery.mixitup.js') }}"></script>     
        <!-- jquery venobox min js -->
            <script src="{{ asset('assets/js/venobox.min.js') }}"></script>        
        <!-- form-contact js -->
            <script src="{{ asset('assets/js/form-contact.js') }}"></script>           
        <!-- jquery appear js -->           
            <script src="{{ asset('assets/js/jquery.appear.js') }}"></script>          
        <!-- WOW - Reveal Animations When You Scroll -->
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <!-- switcher js -->
        <script src="{{ asset('assets/js/switcher.js') }}"></script>           
        <!-- scripts js -->
        <script src="{{ asset('assets/js/scripts.j') }}s"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/580326369ca1830bdc93b7b9/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
    </body>

<!-- Mirrored from themesvila.com/html-templates/greatway/greatway/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Oct 2016 17:00:08 GMT -->
</html>
